﻿namespace Heartbeat
{
    public class HeartbeatInfo
    {
#pragma warning disable CA1819 // Properties should not return arrays

        public UpdateInfo UpdateInfo { get; set; }
        public Endpoint[] Endpoints { get; set; }
        public SensorsInfo[] SensorsInfo { get; set; }

#pragma warning restore CA1819 // Properties should not return arrays
    }
}
