﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Heartbeat
{
    public class UpdateInfo
    {
        public string Version { get; set; }
        public string Address { get; set; }
        public DateTime? UrlValidity { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public UpdateType UpdateType { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Severity Severity { get; set; }
    }

    public enum UpdateType
    {
        Silent,
        Upgrade
    }

    public enum Severity
    {
        Normal,
        Critical
    }
}
